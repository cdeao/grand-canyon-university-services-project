//Cameron Deao
//CST-235
//2/17/2019
package business;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;
import java.util.logging.Logger;
import javax.ejb.TimerService;
import javax.annotation.Resource;
import javax.ejb.Timeout;

@Stateless
public class MyTimerService {

	private static final Logger logger = Logger.getLogger("business.MyTimerService");
	@Resource
	TimerService timerService;
    /**
     * Default constructor. 
     */
    public MyTimerService() {
       
    }
    
    public void setTimer(long interval) {
    	timerService.createTimer(interval, "My Timer");
    }
    
    @Timeout
    public void programmicTimer(Timer timer) {
    	logger.info("TIMEOUT");
    }
	
    @SuppressWarnings("unused")
	@Schedule(second="*/10", minute="*", hour="0-23", dayOfWeek="Mon-Fri",
      dayOfMonth="*", month="*", year="*", info="MyTimer")
    private void scheduledTimeout(final Timer t) {
        logger.info("@Schedule called at: " + new java.util.Date());
    }
}